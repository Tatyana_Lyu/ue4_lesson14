#include <iostream>
#include <string>

int main()
{

	std::string first = "100500";
	std::cout << first << "\n";
	
	std::cout << "String length is " << first.length() << "\n";
	std::cout << "First symbol is " << first[0] << "\n";
	std::cout << "Last symbol is " << first[first.size()-1] << "\n";

}